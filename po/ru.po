# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the footage package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: footage\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-16 04:53-0400\n"
"PO-Revision-Date: 2023-06-16 20:09+0300\n"
"Last-Translator: Сергей Ворон <voron@duck.com>\n"
"Language-Team: \n"
"Language: ru_RU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.3.1\n"

#: data/io.gitlab.adhami3310.Footage.desktop.in.in:3
#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:5
#: data/resources/blueprints/window.blp:9
#: data/resources/blueprints/window.blp:43 src/main.rs:48 src/window.rs:712
msgid "Footage"
msgstr "Footage"

#: data/io.gitlab.adhami3310.Footage.desktop.in.in:4
msgid "Video Editor"
msgstr "Видео редактор"

#: data/io.gitlab.adhami3310.Footage.desktop.in.in:5
msgid "Polish Your Videos"
msgstr "Улучшайте свои видео"

#: data/io.gitlab.adhami3310.Footage.desktop.in.in:11
msgid "video;edit;trim;crop;convert"
msgstr ""
"video;edit;trim;crop;convert;видео;редактор;обрезать;вырезать;конвертировать"

#: data/io.gitlab.adhami3310.Footage.gschema.xml.in:6
msgid "Window width"
msgstr "Ширина окна"

#: data/io.gitlab.adhami3310.Footage.gschema.xml.in:10
msgid "Window height"
msgstr "Высота окна"

#: data/io.gitlab.adhami3310.Footage.gschema.xml.in:14
msgid "Window maximized state"
msgstr "Окно развернуто"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:6
msgid "Khaleel Al-Adhami"
msgstr "Khaleel Al-Adhami"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:7
msgid "Polish your videos"
msgstr "Улучшайте свои видео"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:13
msgid ""
"Trim, flip, rotate and crop individual clips. Footage is a useful tool for "
"quickly editing short videos and screencasts. It's also capable of exporting "
"any video into a format of your choice."
msgstr ""
"Обрезайте, переворачивайте, поворачивайте и обрезайте видео клипы. Footage — "
"полезный инструмент для быстрого редактирования коротких видеороликов и "
"скринкастов. Он также может экспортировать любое видео в любой формат по "
"вашему выбору."

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:28
msgid "video"
msgstr "видео"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:29
msgid "edit"
msgstr "редактировать"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:30
msgid "trim"
msgstr "обрезка"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:31
msgid "crop"
msgstr "вырезать"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:32
msgid "convert"
msgstr "конвертировать"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:48
msgid "Editing screen with default options"
msgstr "Экран редактирования с параметрами по умолчанию"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:68
msgid ""
"This minor release introduces a couple of bug fixes and enhances keyboard "
"accessibility."
msgstr ""
"В этом минорном выпуске исправлено несколько ошибок и улучшены возможности "
"клавиатуры."

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:73
msgid "Initial version."
msgstr "Первоначальная версия."

#: data/resources/blueprints/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Общие"

#: data/resources/blueprints/help-overlay.blp:14
msgctxt "shortcut window"
msgid "New Window"
msgstr "Новое окно"

#: data/resources/blueprints/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Показать комбинации клавиш"

#: data/resources/blueprints/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Quit"
msgstr "Выйти"

#: data/resources/blueprints/crop.blp:24
msgid "Top left crop corner"
msgstr "Верхний левый угол обрезки"

#: data/resources/blueprints/crop.blp:34
msgid "Top right crop corner"
msgstr "Верхний правый угол обрезки"

#: data/resources/blueprints/crop.blp:49
msgid "Bottom left crop corner"
msgstr "Нижний левый угол обрезки"

#: data/resources/blueprints/crop.blp:59
msgid "Bottom right crop corner"
msgstr "Нижний правый угол обрезки"

#: data/resources/blueprints/window.blp:27
msgid "Primary Menu"
msgstr "Основное меню"

#: data/resources/blueprints/window.blp:44
msgid "Choose a video to edit"
msgstr "Выберите видео для редактирования"

#: data/resources/blueprints/window.blp:55
#: data/resources/blueprints/window.blp:363
msgid "Open File…"
msgstr "Открыть файл…"

#: data/resources/blueprints/window.blp:77
msgid "Could not Load Video"
msgstr "Не удалось загрузить видео"

#: data/resources/blueprints/window.blp:78
msgid "This video could be corrupted or may use an unsupported file format."
msgstr ""
"Это видео может быть повреждено или использует неподдерживаемый формат файла."

#: data/resources/blueprints/window.blp:124
msgid "Resize"
msgstr "Изменить размер"

#: data/resources/blueprints/window.blp:129
#: data/resources/blueprints/window.blp:130
#: data/resources/blueprints/window.blp:138
#: data/resources/blueprints/window.blp:140
msgid "Width"
msgstr "Ширина"

#: data/resources/blueprints/window.blp:147
msgid "Preserve aspect ratio"
msgstr "Сохранить соотношение сторон"

#: data/resources/blueprints/window.blp:152
#: data/resources/blueprints/window.blp:153
#: data/resources/blueprints/window.blp:161
#: data/resources/blueprints/window.blp:162
msgid "Height"
msgstr "Высота"

#: data/resources/blueprints/window.blp:173
msgid "Choose between resizing with percentages or using exact pixels"
msgstr ""
"Выберите между изменением размера в процентах или использованием точных "
"пикселей"

#: data/resources/blueprints/window.blp:177
msgid "%"
msgstr "%"

#: data/resources/blueprints/window.blp:177
msgid "px"
msgstr "px"

#: data/resources/blueprints/window.blp:186
msgid "Framerate"
msgstr "Частота кадров"

#: data/resources/blueprints/window.blp:204
msgid "Container Format"
msgstr "Формат контейнера"

#: data/resources/blueprints/window.blp:210
msgid "Video Encoding"
msgstr "Видеокодек"

#: data/resources/blueprints/window.blp:216
msgid "Audio Encoding"
msgstr "Аудиокодек"

#: data/resources/blueprints/window.blp:227
msgid "Save Video"
msgstr "Сохранить видео"

#: data/resources/blueprints/window.blp:254
msgid "Video Exported"
msgstr "Видео экспортировано"

#: data/resources/blueprints/window.blp:264
msgid "Open Video"
msgstr "Открыть видео"

#: data/resources/blueprints/window.blp:276
msgid "Finish"
msgstr "Готово"

#: data/resources/blueprints/window.blp:298
msgid "Exporting Video Unsuccessful"
msgstr "Не удалось экспортировать видео"

#: data/resources/blueprints/window.blp:307
msgid "Retry"
msgstr "Повторить"

#: data/resources/blueprints/window.blp:327
msgid "Rendering…"
msgstr "Рендеринг…"

#: data/resources/blueprints/window.blp:329
msgid "This may take a while"
msgstr "Это может занять некоторое время"

#: data/resources/blueprints/window.blp:344 src/window.rs:458 src/window.rs:484
msgid "_Cancel"
msgstr "_Отмена"

#: data/resources/blueprints/window.blp:368
msgid "New Window"
msgstr "Новое окно"

#: data/resources/blueprints/window.blp:375
msgid "Keyboard Shortcuts"
msgstr "Комбинации клавиш"

#: data/resources/blueprints/window.blp:380
msgid "About Footage"
msgstr "О Footage"

#: data/resources/blueprints/timeline.blp:18
msgid "Left trimming handle"
msgstr "Левый указатель обрезки"

#: data/resources/blueprints/timeline.blp:52
msgid "Right trimming handle"
msgstr "Правый указатель обрезки"

#: src/window.rs:454 src/window.rs:479
msgid "Stop rendering?"
msgstr "Остановить рендеринг?"

#: src/window.rs:455 src/window.rs:480
msgid "You will lose all progress."
msgstr "Вы потеряете весь прогресс."

#: src/window.rs:459 src/window.rs:484
msgid "_Stop"
msgstr "_Остановить"

#. Translators: Replace "translator-credits" with your names, one name per line
#: src/window.rs:719
msgid "translator-credits"
msgstr "Сергей Ворон <voron.s.a@gmail.com>"

#: src/profiles.rs:78
msgid "Recommended (WEBM, AV1, Opus)"
msgstr "Рекомендуется (WEBM, AV1, Opus)"

#: src/profiles.rs:79
msgid "Keep as-is"
msgstr "Оставить как есть"
